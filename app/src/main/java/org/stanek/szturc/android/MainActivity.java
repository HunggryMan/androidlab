package org.stanek.szturc.android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.stanek.szturc.android.core.Figura;
import org.stanek.szturc.android.core.Kolo;
import org.stanek.szturc.android.core.Kwadrat;
import org.stanek.szturc.android.core.Trojkat;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private TableLayout tableLayout;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tableLayout = (TableLayout) findViewById(R.id.table);
        List<Figura> figures = generateFigures();
        List<TableRow> tableRows = new ArrayList<>();
        for (Figura figura : figures) {
            tableRows.add(generatTableRow(figura));
        }
        for (TableRow tableRow : tableRows) {
            tableLayout.addView(tableRow);
        }

    }


    private TableRow generatTableRow(Figura figure) {

        TableRow tableRow = new TableRow(getApplicationContext());
        TextView figureType = new TextView(getApplicationContext());
        TextView figureCharacteristicData = new TextView(getApplicationContext());
        TextView figureField = new TextView(getApplicationContext());
        ImageView figureIcon = new ImageView(getApplicationContext());
        NumberFormat format = new DecimalFormat("#0.000");
        switch (figure.czymJestem()) {
            case "KWADRAT":
                figureIcon.setImageResource(R.drawable.square);
                break;
            case "TROJKAT":
                figureIcon.setImageResource(R.drawable.triangle);
                break;
            case "KOLO":
                figureIcon.setImageResource(R.drawable.circle);
                break;
        }
        figureField.setText(String.valueOf(format.format(figure.pole())));
        String data =  figure.jakiJestTypMojegoWymiaruLiniowego() + " " +
                String.valueOf(format.format(figure.getWymiarLiniowy()));
        figureCharacteristicData.setText(data);
        tableRow.addView(figureIcon);
        tableRow.addView(figureType);
        tableRow.addView(figureField);
        tableRow.addView(figureCharacteristicData);
        return tableRow;
    }

    private List<Figura> generateFigures() {
        Random generator = new Random();
        int N = 50;
        List<Figura> figury = new ArrayList<>(N); //tablica figur
        for (int i = 0; i < N; i++) {
            int figuraId = generator.nextInt(3);
            if (figuraId == 0) {
                figury.add(new Kwadrat(generator.nextFloat()));
            } else if (figuraId == 1) {
                figury.add(new Kolo(generator.nextFloat()));
            } else if (figuraId == 2) {
                figury.add(new Trojkat(generator.nextFloat()));

            }
        }
        return figury;
    }
}
