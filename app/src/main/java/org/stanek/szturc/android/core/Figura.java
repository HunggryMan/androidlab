package org.stanek.szturc.android.core;

public abstract class Figura {


	public Figura(float wymiarLiniowy) {
		this.wymiarLiniowy = wymiarLiniowy;
	}

	private float wymiarLiniowy;

	public abstract String czymJestem();

	public abstract String jakiJestTypMojegoWymiaruLiniowego ();

	public float getWymiarLiniowy() {
		return wymiarLiniowy;
	}


	 public abstract double pole();

	
	
}
