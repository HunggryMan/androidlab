package org.stanek.szturc.android.core;

/**
 * @author Michał Szturc
 */
public class Kolo extends Figura {

    public Kolo(float wymiarLiniowy) {
        super(wymiarLiniowy);
    }

    @Override
    public double pole() {
        return Math.pow(super.getWymiarLiniowy(), 2) * Math.PI;
    }

    public double promien() {
        return super.getWymiarLiniowy() * 2;
    }

    @Override
    public String czymJestem() {
        return "KOLO";
    }

    @Override
    public String jakiJestTypMojegoWymiaruLiniowego() {
        return "ŚREDNICA";
    }
}
