package org.stanek.szturc.android.core;


public class Kwadrat extends Figura {

	public Kwadrat(float wymiarLiniowy){
		super(wymiarLiniowy);
	}
	

	@Override
	public double pole() {
		return super.getWymiarLiniowy() * super.getWymiarLiniowy();
	}

	public double przekatna (){
		return super.getWymiarLiniowy() * 1.41;
	}


	@Override
	public String czymJestem() {
		return "KWADRAT";
	}

	@Override
	public String jakiJestTypMojegoWymiaruLiniowego() {
		return "PRZEKATNA";
	}
}
