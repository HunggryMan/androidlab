package org.stanek.szturc.android.core;

/**
 * @author Michał Szturc
 */
public class Trojkat extends Figura {

    public Trojkat(float wymiarLiniowy) {
        super(wymiarLiniowy);
    }

    @Override
    public double pole() {
        return Math.pow(super.getWymiarLiniowy(), 2) * Math.sqrt(3) * 0.25;
    }

    public double wysokosc() {
        return super.getWymiarLiniowy() * Math.sqrt(3) * 0.5;
    }

    @Override
    public String czymJestem() {
        return "TROJKAT";
    }

    @Override
    public String jakiJestTypMojegoWymiaruLiniowego() {
        return "WYSOKOŚĆ";
    }
}
